﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponRotation : MonoBehaviour
{

    [SerializeField] private float maxAngleDeflection = 30f;
    [SerializeField] private float speedOfPendulum = 1f;
    [SerializeField] private float myXRotation;

    [Header("Information About Axis")]

    [SerializeField] private float setCurrentAxis;
    [SerializeField] private string ChooseAxis;

    private bool active = false;

    private float angle;

    void FixedUpdate()
    {
        if (active)
        {
            if (Mathf.Abs(angle) + 1 < 90)
            {
                angle += maxAngleDeflection * Mathf.Sin(Time.deltaTime * speedOfPendulum);
                transform.localRotation = Quaternion.Euler(myXRotation, 90, angle + setCurrentAxis);
            }
            else
                active = false;

            //transform.localRotation = Quaternion.Euler(0, 0, 0);
        }
    }

    public void Active()
    {
        active = true;
    }
}
