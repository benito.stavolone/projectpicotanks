﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackGroundMenuAnimation : MonoBehaviour
{

    [SerializeField] private float speed;

    private Vector3 firstDirection;
    private Vector3 secondDirection;

    private int lifetime = 5;

    private void Start()
    {
        MoveRightDown();
    }

    private void Update()
    {
        transform.position += (firstDirection + secondDirection) * speed * Time.deltaTime;
    }

    private void MoveRightDown()
    {
        firstDirection = Vector3.right;
        secondDirection = Vector3.down;
        Invoke("MoveRightUp",lifetime);
    }

    private void MoveRightUp()
    {
        firstDirection = Vector3.right;
        secondDirection = Vector3.up;
        Invoke("MoveLeftUp", lifetime);
    }

    private void MoveLeftUp()
    {
        firstDirection = Vector3.left;
        secondDirection = Vector3.up;
        Invoke("MoveLeftDown", lifetime);
    }

    private void MoveLeftDown()
    {
        firstDirection = Vector3.left;
        secondDirection = Vector3.down;
        Invoke("MoveRightDown", lifetime);
    }
}
