﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteMovement : MonoBehaviour
{
    private bool isMoving = false;
    [SerializeField] private float speed;

    private void Update()
    {
        if (isMoving)
        {
            transform.position += Vector3.up * speed * Time.deltaTime;
            transform.localScale += transform.localScale * speed/5 * Time.deltaTime;
        }
            
    }

    public void StartToMove()
    {
        isMoving = true;
        Invoke("Reset", 2);
    }

    private void Reset()
    {
        transform.gameObject.SetActive(false);
    }
}
