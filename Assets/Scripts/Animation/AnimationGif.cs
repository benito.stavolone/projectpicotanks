﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnimationGif : MonoBehaviour
{

    public Sprite[] sprites;
    public float framesPerSecond = 0.03f;

    public Image myImage;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        int index = (int)(Time.time * framesPerSecond) % sprites.Length;
        myImage.sprite = sprites[index];
    }
}
