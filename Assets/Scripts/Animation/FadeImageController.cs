﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class FadeImageController : MonoBehaviour
{

    [SerializeField] private Image fadeImage;

    [SerializeField] private Color startColor;
    [SerializeField] private Color endColor;

    [SerializeField] private float duration;

    private bool isFade = false;

    public void Startfade()
    {
        fadeImage.color = startColor;
        fadeImage.gameObject.SetActive(true);
        StartCoroutine(BeginFade());
    }

    IEnumerator BeginFade()
    {
        Debug.Break();
        isFade = true;

        float timer = 0;

        while (timer <= duration)
        {
            fadeImage.color = Color.Lerp(startColor, endColor, timer/duration);
            timer += Time.deltaTime;
            yield return null;
        }

        isFade = false;

        SceneManager.LoadScene(0);
    }
}
