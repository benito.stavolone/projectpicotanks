﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PendulumAnimationController : MonoBehaviour
{

    [SerializeField] private float maxAngleDeflection = 30f;
    [SerializeField] private float speedOfPendulum = 1f;

    [Header("Information About Axis")]
    
    [SerializeField] private float setCurrentAxis;
    [SerializeField] private string ChooseAxis;

    private bool active = false;

    private float angle;

    // Update is called once per frame
    void FixedUpdate()
    {
        if (active)
        {
            if (Mathf.Abs(angle) + 1 < 90)
            {
                angle += maxAngleDeflection * Mathf.Sin(Time.deltaTime * speedOfPendulum );
                transform.localRotation = Quaternion.Euler(transform.localRotation.z, transform.localRotation.y, angle + setCurrentAxis);
            }
            else
                active = false;

            //transform.localRotation = Quaternion.Euler(0, 0, 0);
        }
    }

    public void Active()
    {
        active = true;
    }
}
