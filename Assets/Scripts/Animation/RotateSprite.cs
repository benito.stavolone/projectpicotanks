﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateSprite : MonoBehaviour
{
    private bool timeToRotate = false;

    private void OnEnable()
    {
        timeToRotate = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (timeToRotate)
        {
            transform.RotateAroundLocal(Vector3.up, 5 * Time.deltaTime);
        }
    }

    private void OnDisable()
    {
        timeToRotate = false;
    }
}
