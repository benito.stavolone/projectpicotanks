﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TopDownScript : MonoBehaviour
{

    [SerializeField] private Transform target;

    [SerializeField] private float smoothSpeed = 0.125f;

    [SerializeField] private Vector3 offset;

    void Start()
    {
        HandleCamera();
    }
    void LateUpdate()
    {
        HandleCamera();   
    }

    protected virtual void HandleCamera()
    {
        Vector3 desiredPosition = target.position + offset;
        Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed * Time.deltaTime);
        transform.position = smoothedPosition;
    }
}
