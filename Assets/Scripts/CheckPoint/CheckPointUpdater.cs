﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPointUpdater : MonoBehaviour
{
    [SerializeField] private int checkPointValue;
    [SerializeField] private AudioClip ambientClip;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            CheckPointManager.instance.UpdateCheckPoint(checkPointValue);
            SoundManager.instance.Playbackground(ambientClip);
            gameObject.SetActive(false);
        }
    }
}
