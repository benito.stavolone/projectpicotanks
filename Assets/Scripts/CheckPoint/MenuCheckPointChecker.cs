﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuCheckPointChecker : MonoBehaviour
{

    private static readonly string FirstPlay = "FirstPlay";
    private static readonly string ZoneUnlocked = "ZoneUnlocked";
    private static readonly string CheckPointToSet = "CheckPoint";
    private int firstPlayInt;

    private int checkPointIndex=0;

    [SerializeField] private List<ButtonZone> zones;

    private void Start()
    {
        PlayerPrefs.SetInt("Turret", 0);
        PlayerPrefs.SetInt("Base", 0);
    }

    public void CheckFirstPlay()
    {
        firstPlayInt = PlayerPrefs.GetInt(FirstPlay);

        if (firstPlayInt == 0)
        {
            PlayerPrefs.SetInt(ZoneUnlocked, 0);
            PlayerPrefs.SetInt(FirstPlay, -1);
            DeactivateZoneButton();
        }
        else
        {
            checkPointIndex = PlayerPrefs.GetInt(ZoneUnlocked);
            ActivateZoneButton();
        }
                 
    }

    private void ActivateZoneButton()
    {
        Debug.Log(checkPointIndex);
        for(int i = 0; i < checkPointIndex; i++)
        {
            zones[i].button.interactable = true;
            zones[i].chains.SetActive(false);
        }
    }

    private void DeactivateZoneButton()
    {
        for (int i = 0; i < checkPointIndex; i++)
        {
            zones[i].button.interactable = false;
            zones[i].chains.SetActive(true);
        }
    }

    public void ForestButton()
    {
        SceneManager.LoadScene(1);
        PlayerPrefs.SetInt(CheckPointToSet, 0);
    }

    public void LakeButton()
    {
        SceneManager.LoadScene(1);
        PlayerPrefs.SetInt(CheckPointToSet, 1);
    }

    public void PortButton()
    {
        SceneManager.LoadScene(1);
        PlayerPrefs.SetInt(CheckPointToSet, 2);
    }

    public void ResetCampaign()
    {
        PlayerPrefs.SetInt(FirstPlay, 0);
        CheckFirstPlay();
    }
}

[System.Serializable]
public class ButtonZone
{
    public Button button;
    public GameObject chains;
}
