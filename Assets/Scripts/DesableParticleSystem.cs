﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DesableParticleSystem : MonoBehaviour
{
    private float lifetime = 3;

    private void OnEnable()
    {
        Invoke("Reset", lifetime);
    }

    private void Reset()
    {
        gameObject.SetActive(false);
    }
}
