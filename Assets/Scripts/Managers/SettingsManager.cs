﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class SettingsManager : MonoBehaviour
{
    [SerializeField] private AudioMixer audioMixer;
    [SerializeField] private TMP_Dropdown resolutionsDropDown;

    [SerializeField] private Slider audioSlider;

    public void SetVolume()
    {
        audioMixer.SetFloat("Volume", audioSlider.value);
    }

   
}
