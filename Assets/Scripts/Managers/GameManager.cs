﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public bool isPlaying;

    [SerializeField] private GameObject loadingPanel;

    public enum MissionState
    {
        Null,
        ClearArea,
        SearchingDragObject,
        DragObject,
        DestroyObjects,
        Dominio
    }

    public MissionState currentMissionState;

    private void Awake()
    { 
        instance = this;
    }

    private void Start()
    {
        loadingPanel.SetActive(true);
        isPlaying = false;
        Invoke("ResetPanelLoading", 5);
        currentMissionState = MissionState.Null;
    }

    public void ChangeCurrentMission(MissionState newMission)
    {
        currentMissionState = newMission;
    }

    public bool CheckifDragMission()
    {
        if (currentMissionState == MissionState.DragObject)
            return true;
        else
            return false;
    }

    private void ResetPanelLoading()
    {
        loadingPanel.SetActive(false);
        isPlaying = true;
    }
}
