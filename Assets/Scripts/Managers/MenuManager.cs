﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public enum MenuState
    {
        MainMenu,
        Campaign,
        Settings
    }

    [Header("Panel List")]

    [SerializeField] private GameObject mainMenuPanel;
    [SerializeField] private GameObject campaignPanel;
    [SerializeField] private GameObject settingsPanel;
    [SerializeField] private GameObject surePanel;

    private void Start()
    {
        OnchangeMenuState(MenuState.MainMenu);
    }

    private void OnchangeMenuState(MenuState currentMenuState)
    {
        switch (currentMenuState)
        {
            case MenuState.MainMenu:
                mainMenuPanel.SetActive(true);
                campaignPanel.SetActive(false);
                settingsPanel.SetActive(false);
                break;

            case MenuState.Campaign:
                mainMenuPanel.SetActive(false);
                campaignPanel.SetActive(true);
                settingsPanel.SetActive(false);
                surePanel.SetActive(false);
                break;

            case MenuState.Settings:
                mainMenuPanel.SetActive(false);
                campaignPanel.SetActive(false);
                settingsPanel.SetActive(true);
                break;
        }
    }

    public void ButtonCampaign()
    {
        OnchangeMenuState(MenuState.Campaign);
    }

    public void ButtonSettings()
    {
        OnchangeMenuState(MenuState.Settings);
    }

    public void BackButton()
    {
        OnchangeMenuState(MenuState.MainMenu);
    }

    public void NewCampaignButton()
    {
        surePanel.SetActive(true);
    }

    public void IAmNotSure()
    {
        surePanel.SetActive(false);
    }
}
