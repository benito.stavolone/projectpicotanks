﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPointManager : MonoBehaviour
{
    public static CheckPointManager instance;

    [SerializeField] private Transform[] checkPointPosition;
    [SerializeField] private GameObject[] zones;

    [SerializeField] private AudioClip[] backgroundZones;

    [SerializeField] private Transform Player;

    private int myCheckPoint;

    int checkPointIndex = 0;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        LastCheckPoint();
    }

    public void LastCheckPoint()
    {
        myCheckPoint = PlayerPrefs.GetInt("CheckPoint");
        checkPointIndex = myCheckPoint;
        Player.position = checkPointPosition[myCheckPoint].position;

        SoundManager.instance.Playbackground(backgroundZones[myCheckPoint]);

        DisablePreviousZones();
    }

    public void UpdateCheckPoint(int value)
    {
        SoundManager.instance.Playbackground(backgroundZones[myCheckPoint]);
        int zoneUnlocked = PlayerPrefs.GetInt("ZoneUnlocked");
        if(value > zoneUnlocked)
            PlayerPrefs.SetInt("ZoneUnlocked", value);
        PlayerPrefs.SetInt("CheckPoint", value);
        Debug.Log(value);
    }

    private void DisablePreviousZones()
    {
        for(int i = 0; i < myCheckPoint; i++)
        {
            zones[i].SetActive(false);
        }
    }
}
