﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{

    public static UIManager instance;

    [SerializeField] private Button infoTurretButton;
    [SerializeField] private Button infoBaseButton;

    [Header("Mission")]

    [SerializeField] private TextMeshProUGUI missionText;

    [Header("Defeat Enemy")]

    [SerializeField] private TextMeshProUGUI numberTotal;
    [SerializeField] private TextMeshProUGUI currentNumber;

    [Header("Pause")]

    [SerializeField] private GameObject pausePanel;
    [SerializeField] private GameObject pauseButton;

    [Header("GameOver")]

    [SerializeField] private GameObject gameOverPanel;

    [SerializeField] private WeaponController weaponController;

    private void Awake()
    {
        instance = this;
    }

    public void ShowInfoButton(Button button)
    {
        button.gameObject.SetActive(true);
    }

    public void HideInfoButton(Button button)
    {
        button.gameObject.SetActive(false);
    }

    public void ShowInfoPanel(GameObject panel)
    {
        panel.gameObject.SetActive(true);
    }

    public void HidePanel(GameObject panel)
    {
        panel.gameObject.SetActive(false);
    }

    public void SetMissiontext(string newText)
    {
        missionText.text = newText;
    }

    public void ShowNumberTotalofEnemy(int value)
    {
        numberTotal.text = " / " + value.ToString();
    }

    public void ShowCurrentNumberOfEnemy(int value)
    {
        currentNumber.text = value.ToString();
    }

    public void ShowTextMissionsDefeat()
    {
        numberTotal.gameObject.SetActive(true);
        currentNumber.gameObject.SetActive(true);
    }

    public void HideTextMissionsDefeat()
    {
        numberTotal.gameObject.SetActive(false);
        currentNumber.gameObject.SetActive(false);
    }

    public void PauseButton()
    {
        pausePanel.SetActive(true);
        pauseButton.SetActive(false);
        Time.timeScale = 0;
    }

    public void ReturnToGame()
    {
        pausePanel.SetActive(false);
        pauseButton.SetActive(true);
        Time.timeScale = 1;
    }

    public void ReturnToMenu()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(0);
    }

    public void ShowGameOverPanel()
    {
        gameOverPanel.SetActive(true);
    }

    public void Restart()
    {
        PlayerPrefs.SetInt("Turret", 0);
        PlayerPrefs.SetInt("Base", 0);
        PlayerPrefs.SetInt("CheckPoint", 0);
        SceneManager.LoadScene(1);
    }

    public void LastCheckPoint()
    {
        PlayerPrefs.SetInt("Turret",weaponController.GetActualTurret());
        PlayerPrefs.SetInt("Base",weaponController.GetActualBase());
        SceneManager.LoadScene(1);
    }

}
