﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static SoundManager instance;

    [SerializeField] private AudioSource backgroundAudio;
    [SerializeField] private AudioSource engineAudio;
    [SerializeField] private AudioSource effectAudio;
    [SerializeField] private AudioSource enemyEffectAudio;
    [SerializeField] private AudioSource explosionAudio;

    private void Awake()
    {
        instance = this;
    }

    public void Playbackground(AudioClip clip)
    {
        backgroundAudio.clip = clip;
        backgroundAudio.Play();
    }

    public void PlayEngines(AudioClip clip)
    {
        engineAudio.clip = clip;
        engineAudio.Play();
    }

    public void ChangeEnginePitch(float pitch)
    {
        engineAudio.pitch = pitch;
    }

    public void SoundEffect(AudioClip clip)
    {
        effectAudio.clip = clip;
        effectAudio.Play();
    }

    public void EnemySoundEffect(AudioClip clip)
    {
        enemyEffectAudio.clip = clip;
        enemyEffectAudio.Play();
    }

    public void ExplosionsoundEffect(AudioClip clip)
    {
        explosionAudio.clip = clip;
        explosionAudio.Play();
    }
}
