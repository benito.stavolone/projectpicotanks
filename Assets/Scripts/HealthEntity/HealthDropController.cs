﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthDropController : MonoBehaviour
{
    public int healValue = 10;
    private bool isFullofLife = false;
    public AudioClip soundEffect;

    private void OnTriggerEnter(Collider collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            isFullofLife =  collision.gameObject.GetComponent<LivingEntity>().UpdateHealthValue(healValue);
            if (!isFullofLife)
            {
                gameObject.SetActive(false);
                SoundManager.instance.SoundEffect(soundEffect);
                Component particle = ObjectPool.instance.SpawnFromPool<Component>("Medikit", collision.gameObject.transform.position, Quaternion.Euler(-90, 0, 0));
                particle.transform.SetParent(collision.gameObject.transform);

            }
                
        }
    }
}
