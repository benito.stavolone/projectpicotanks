﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LivingEntity : MonoBehaviour, IDamageable
{
    [Header("Health")]

    [SerializeField] protected float startinHealth;

    public float StartinHealth
    {
        get { return startinHealth; }
        set { startinHealth = value; }
    }

    [SerializeField] protected Slider slider;

    protected float _health;
    protected bool _dead;

    public event System.Action OnDeath;

    protected void Start()
    {
        _health = startinHealth;
    }

    public virtual void TakeHit(float damage, Vector3 hitPoint, Vector3 hitDirection)
    {
        TakeDamage(damage);
    }

    public virtual void TakeDamage(float damage)
    {
        float newDamage = startinHealth * damage / startinHealth;
        _health -= newDamage;
        slider.value = _health / startinHealth;
        if (_health <= 0 && _dead == false)
        {
            Die();
        }
    }

    protected void Die()
    {
        _dead = true;
        OnDeath?.Invoke();
    }

    public bool UpdateHealthValue(int value)
    {
        if (_health != startinHealth)
        {
            if (_health + value >= startinHealth)
                _health = startinHealth;
            else
                _health += value;

            slider.value = _health / startinHealth;
            return false;
        }
        else
            return true;
        
    }

    public void RestoreHealth()
    {
        _health = startinHealth;
        slider.value = _health;
        _dead = false;
    }
}
