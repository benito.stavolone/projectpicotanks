﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;
using UnityEngine;

public class BarrierController : LivingEntity
{
    private LivingEntity myLivingEntity;

    private void Start()
    {
        base.Start();
        myLivingEntity = GetComponent<LivingEntity>();
        myLivingEntity.OnDeath += OnMyDeath;
    }

    private void OnMyDeath()
    {
        Destroy(gameObject);
    }
}
