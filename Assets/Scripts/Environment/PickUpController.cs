﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.UI;

public class PickUpController : MonoBehaviour
{
    [SerializeField] private GameObject[] myObjects;
    [SerializeField] private Transform turretHolder;

    [SerializeField] private Button infoButton;

    [SerializeField] private GameObject[] panelList;
    [SerializeField] private GameObject[] panelListNotequiped;

    [SerializeField] private AudioClip switchObject;

    private TankController tankController;
    [SerializeField] private WeaponController weaponController;
    private GameObject myObject;
    private int index = 0;
    private Collider collider;
    private bool isEquiped;

    private UIManager UIReference;

    private void Start()
    {
        UIReference = UIManager.instance;

        for(int i = 0; i < myObjects.Length; i++)
        {
            if (myObjects[i].activeSelf)
                myObject = myObjects[i];
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            UIReference.ShowInfoButton(infoButton);
            isEquiped = weaponController.CheckSameTurret(myObject);
        }

        collider = other;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            UIReference.HideInfoButton(infoButton);
            if (!isEquiped)
                UIReference.HidePanel(panelList[index]);
            else
                UIReference.HidePanel(panelListNotequiped[index]);
        }          
    }

    public void InfoButtonClick()
    {
        UIReference.HideInfoButton(infoButton);

        tankController = collider.gameObject.GetComponent<TankController>();

        index = weaponController.GetPanelID(myObject);

        if (!isEquiped)
            UIReference.ShowInfoPanel(panelList[index]);
        else
            UIReference.ShowInfoPanel(panelListNotequiped[index]);
    }

    public void EquipButton()
    {
        SoundManager.instance.SoundEffect(switchObject);
        Component myparticle = ObjectPool.instance.SpawnFromPool<Component>("ChangeWeapon", turretHolder.position + Vector3.down, Quaternion.Euler(-90, 0, 0));
        myparticle.transform.SetParent(turretHolder);
        myObject.SetActive(false);
        myObject = weaponController.GetWeaponId(myObject);

        for (int i = 0; i < myObjects.Length; i++)
        {
            if (myObjects[i].name == myObject.name)
                myObjects[i].SetActive(true);
            else
                myObjects[i].SetActive(false);
        }

        UIReference.HidePanel(panelList[index]);
        UIReference.ShowInfoButton(infoButton);
    }
}
