﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlphaChangedController : MonoBehaviour
{
    [SerializeField] private Material newMaterial;

    private Material myMaterial;

    private void Start()
    {
        myMaterial = GetComponent<MeshRenderer>().material;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
            gameObject.GetComponent<MeshRenderer>().material = newMaterial;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
            gameObject.GetComponent<MeshRenderer>().material = myMaterial;
    }
}
