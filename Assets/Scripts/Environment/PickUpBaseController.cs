﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PickUpBaseController : MonoBehaviour
{

    [SerializeField] private GameObject[] myObjects;
    [SerializeField] private GameObject[] panelList;
    [SerializeField] private GameObject[] panelListNotequiped;
    [SerializeField] private Transform baseHolder;

    [SerializeField] private Button infoButton;

    private UIManager UIReference;
    private GameObject myObject;
    private int index = 0;
    private Collider collider;
    private GameObject panel = null;
    private bool isEquiped;

    private TankController tankController;
    [SerializeField] private WeaponController weaponController;

    [SerializeField] private AudioClip switchObject;

    private void Start()
    {
        UIReference = UIManager.instance;

        for (int i = 0; i < myObjects.Length; i++)
        {
            if (myObjects[i].activeSelf)
                myObject = myObjects[i];
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            UIReference.ShowInfoButton(infoButton);
            isEquiped = weaponController.CheckSameEquipment(myObject);
        }   
        collider = other;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            UIReference.HideInfoButton(infoButton);
                if(!isEquiped)
                    UIReference.HidePanel(panelList[index]);
                else
                    UIReference.HidePanel(panelListNotequiped[index]);
                
        }
    }

    public void InfoButtonClick()
    {
        UIReference.HideInfoButton(infoButton);

        tankController = collider.gameObject.GetComponent<TankController>();

        index = weaponController.GetPanelIDBase(myObject);

        if(!isEquiped)
            UIReference.ShowInfoPanel(panelList[index]);
        else
            UIReference.ShowInfoPanel(panelListNotequiped[index]);
    }

    public void EquipButton()
    {
        SoundManager.instance.SoundEffect(switchObject);
        Component myparticle = ObjectPool.instance.SpawnFromPool<Component>("ChangeWeapon", baseHolder.position + Vector3.down, Quaternion.Euler(-90,0,0));
        myparticle.transform.SetParent(baseHolder);
        myObject.SetActive(false);
        myObject = weaponController.GetBaseID(myObject);

        for (int i = 0; i < myObjects.Length; i++)
        {
            if (myObjects[i].name == myObject.name)
                myObjects[i].SetActive(true);
            else
                myObjects[i].SetActive(false);
        }

        UIReference.HidePanel(panelList[index]);
        UIReference.ShowInfoButton(infoButton);
    }
}
