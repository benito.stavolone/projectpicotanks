﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class BaseController : MonoBehaviour
{
    [SerializeField] private TankController tankController;

    [SerializeField] private float speed;

    [SerializeField] private float armor;

    [SerializeField] private AudioClip tankEngineSound;

    private void OnEnable()
    {
        tankController.TankSpeed = speed;
        tankController.TankArmor = armor;
        tankController.TankSound = tankEngineSound;
    }
}
