﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(InputTankController))]

public class TankController : LivingEntity
{
    private TurretController turretController;
    private BaseController baseController;
    private Vector3 turretFinalLookDirection;
    private LivingEntity _myLivingEntity;

    private Rigidbody rb;

    [SerializeField] private GameObject particleSystemSmoke;

    [SerializeField] private AudioClip backGroundSound;

    [SerializeField] private Transform objectToray1;
    [SerializeField] private Transform objectToray2;
    [SerializeField] private Transform objectToray3;

    public TurretController TurretController
    {
        get
        {
            return turretController;
        }

        set
        {
            turretController = value;
        }
    }

    [Header("Player Movements")]

    [SerializeField] private float tankRotationSpeed = 20f;
    //[SerializeField] private float tankSpeed = 15f;

    [Header("Turret Propertis")]

    [SerializeField] private GameObject turret;
    [SerializeField] private float turretLagSpeed = 0.5f;

    bool isShot = false;
    bool isDeadZone = false;
    bool canShot = false;

    [Header("Joystick")]

    public FixedJoystick moveJoystick;
    public FixedJoystick lookJoystick;

    private Vector3 playerRotation;
    private Vector3 lookAtPosition;

    [Header("Damage Sprite")]

    [SerializeField] private GameObject redBorder;

    //Reference to Base

    private float tankSpeed;

    private float distanceFromWall = 1.5f;
    private float distanceFromEnemy = 1.5f;
    [SerializeField] private LayerMask layerWall;
    [SerializeField] private LayerMask layerEnemy;

    private bool ImAlive = true;

    public float TankSpeed
    {
        get { return tankSpeed; }
        set { tankSpeed = value; }
    }

    private float tankArmor;

    public float TankArmor
    {
        get { return tankArmor; }
        set { tankArmor = value; }
    }

    private AudioClip tankSound;

    public AudioClip TankSound
    {
        get { return tankSound; }
        set 
        { 
            tankSound = value;
            if(SoundManager.instance!=null)
                SoundManager.instance.PlayEngines(tankSound);
        }
    }

    private Vector3 PlayerRotation
    {
        set
        {
            if (playerRotation != value)
            {
                if (value == Vector3.zero && canShot==true)
                {
                    turretController.OnTriggerRelease();
                    canShot = false;
                }                 
            }

            playerRotation = value;
        }
    }


    new void Start()
    {
        base.Start();
        rb = GetComponent<Rigidbody>();
        turretController = turret.GetComponentInChildren<TurretController>();
        baseController = GetComponent<BaseController>();
        _myLivingEntity = GetComponent<LivingEntity>();
        _myLivingEntity.OnDeath += OnMyDeath;
        SoundManager.instance.PlayEngines(tankSound);
    }

    private void Update()
    {
        //Shooting();     
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (GameManager.instance.isPlaying)
        {
            if (ImAlive)
            {
                UpdateLookJoystick();
                Ray ray = new Ray(transform.position, transform.forward);

                UpdateMoveJoystick();
                Movement();

                //Rotation();
            }
        }
    }

    private void UpdateMoveJoystick()
    {
        float hoz = moveJoystick.Horizontal;
        float ver = moveJoystick.Vertical;
        Vector3 direction = new Vector3(hoz, 0, ver).normalized;

        if ((!(Physics.Raycast(objectToray1.position, transform.forward, distanceFromWall, layerWall)))   
            && (!(Physics.Raycast(objectToray2.position, transform.forward, distanceFromWall, layerWall))) 
            && (!(Physics.Raycast(transform.position, transform.forward, distanceFromWall, layerWall))))
        {
            if ((!(Physics.Raycast(objectToray1.position, transform.forward, distanceFromWall, layerEnemy)))
    && (!(Physics.Raycast(objectToray2.position, transform.forward, distanceFromWall, layerEnemy)))
    && (!(Physics.Raycast(transform.position, transform.forward, distanceFromWall, layerEnemy))))

                transform.Translate(direction * tankSpeed * Time.deltaTime, Space.World);
        }

        if (ver != 0 && hoz != 0 )
            SoundManager.instance.ChangeEnginePitch(1.5f);
        else
            SoundManager.instance.ChangeEnginePitch(1);

        lookAtPosition = transform.position + direction;
        transform.LookAt(lookAtPosition);
    }

    private void Movement()
    {
        float hoz = Input.GetAxis("Horizontal");
        float ver = Input.GetAxis("Vertical");


        if ((!(Physics.Raycast(objectToray1.position, transform.forward, distanceFromWall, layerWall)))
    && (!(Physics.Raycast(objectToray2.position, transform.forward, distanceFromWall, layerWall)))
    && (!(Physics.Raycast(transform.position, transform.forward, distanceFromWall, layerWall))))
        {
            if ((!(Physics.Raycast(objectToray1.position, transform.forward, distanceFromWall, layerEnemy)))
    && (!(Physics.Raycast(objectToray2.position, transform.forward, distanceFromWall, layerEnemy)))
    && (!(Physics.Raycast(transform.position, transform.forward, distanceFromWall, layerEnemy))))
            {
                Vector3 direction = transform.position + (transform.forward * ver * tankSpeed * Time.deltaTime);
                rb.MovePosition(direction);
            }            
        }

        if(ver !=0)
            SoundManager.instance.ChangeEnginePitch(1.5f);
        else
            SoundManager.instance.ChangeEnginePitch(1);

        Quaternion lookAtPosition = transform.rotation * Quaternion.Euler(Vector3.up * tankRotationSpeed * hoz * Time.deltaTime);
        rb.MoveRotation(lookAtPosition);
    }

    private void Rotation()
    {
        Ray screenray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        Vector3 mousePosition = Vector3.zero;
        Vector3 mouseNormal;

        if (Physics.Raycast(screenray, out hit))
        {
            mousePosition = hit.point;
            mouseNormal = hit.normal;
        }

        Vector3 turretLookRotation = mousePosition - turret.transform.position;
        turretLookRotation.y = 0;
        turretFinalLookDirection = Vector3.Lerp(turretFinalLookDirection,turretLookRotation, Time.deltaTime * turretLagSpeed );

        turret.transform.rotation = Quaternion.LookRotation(turretLookRotation);
    }

    void UpdateLookJoystick()
    {
        if (turret.transform)
        {
            float hoz = lookJoystick.Horizontal;
            float ver = lookJoystick.Vertical;

            PlayerRotation = Vector3.right * hoz +
                                Vector3.forward * ver;

            if (hoz != 0 && ver != 0)
            {
                if (Mathf.Abs(hoz) < 0.4f && Mathf.Abs(ver) < 0.4f) 
                {
                    isDeadZone = true;
                    turretController.AutoAim(turret.transform.rotation);
                }
                else
                {
                    isDeadZone = false;
                }


                if (Mathf.Abs(hoz) >= 0.6f || Mathf.Abs(ver) >= 0.6f)
                {
                    if (playerRotation.sqrMagnitude > 0.0f)
                        turret.transform.rotation = Quaternion.LookRotation(playerRotation, Vector3.up);

                    canShot = true;
                    turretController.OnTriggerHold();
                }
                else
                {
                    turretController.DisableLineRenderer();
                    canShot = false;
                }
                    
            }
        }
    }

    public override void TakeDamage(float damage)
    {
        base.TakeDamage(damage * tankArmor / 10);
        redBorder.SetActive(true);
        Invoke("Reset", 2);
        if (slider.value <= (_myLivingEntity.StartinHealth / 4) / 100)
            particleSystemSmoke.SetActive(true);
        else
            particleSystemSmoke.SetActive(false);
    }

    public override void TakeHit(float damage, Vector3 hitPoint, Vector3 hitDirection)
    {
        base.TakeHit(damage, hitPoint, hitDirection);
        ObjectPool.instance.SpawnFromPool<Component>("TankDamage", hitPoint, Quaternion.LookRotation(hitDirection));
    }

    private void Reset()
    {
        redBorder.SetActive(false);
    }

    private void Shooting()
    {
        if (Input.GetMouseButton(0))
            turretController.OnTriggerHold();

        if (Input.GetMouseButtonUp(0))
            turretController.OnTriggerRelease();
    }

    private void OnMyDeath()
    {
        UIManager.instance.ShowGameOverPanel();
        ImAlive = false;
        SoundManager.instance.SoundEffect(backGroundSound);
        _myLivingEntity.OnDeath -= OnMyDeath;
    }

    public void RestoreHealthonRestart()
    {
        _myLivingEntity.RestoreHealth();
        particleSystemSmoke.SetActive(false);
        _myLivingEntity.OnDeath += OnMyDeath;
    }


}
