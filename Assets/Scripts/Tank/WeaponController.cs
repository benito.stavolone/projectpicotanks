﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponController : MonoBehaviour
{
    [SerializeField] private WeaponAvailable[] weapons;
    [SerializeField] private BaseAvailable[] bases;

    private int baseIndex=0;
    private int turretIndex=0;

    private TankController tankController;

    private void Start()
    {
        tankController = GetComponent<TankController>();
        ActiveCurrentTurret();
        ActiveCurrentBase();
    }

    private void ActiveCurrentTurret()
    {
        int indexTurret = PlayerPrefs.GetInt("Turret");

        for(int i = 0; i < weapons.Length; i++)
        {
            if (i == indexTurret)
            {
                weapons[i].weapon.gameObject.SetActive(true);
                tankController.TurretController = weapons[i].weapon;
            }              
            else
                weapons[i].weapon.gameObject.SetActive(false);
        }
    }

    private void ActiveCurrentBase()
    {
        int indexBase = PlayerPrefs.GetInt("Base");

        for (int i = 0; i < bases.Length; i++)
        {
            if (i == indexBase)
                bases[i].tankbase.gameObject.SetActive(true);
            else
                bases[i].tankbase.gameObject.SetActive(false);
        }
    }

    public bool CheckSameEquipment(GameObject gameObject)
    {
        GameObject currentActive = null;

        for(int i = 0; i < bases.Length; i++)
        {
            if (bases[i].tankbase.activeSelf)
                currentActive = bases[i].tankbase;
        }

        if (currentActive.name == gameObject.name)
            return true;
        else
            return false;
    }

    public bool CheckSameTurret(GameObject gameObject)
    {
        GameObject currentActive = null;

        for (int i = 0; i < weapons.Length; i++)
        {
            if (weapons[i].weapon.gameObject.activeSelf)
                currentActive = weapons[i].weapon.gameObject;
        }

        if (currentActive.name == gameObject.name)
            return true;
        else
            return false;
    }

    public GameObject GetWeaponId(GameObject gameObject)
    {
        GameObject objectToReturn = null;

        for (int i = 0; i < weapons.Length; i++)
        {
            if (weapons[i].weapon.name == gameObject.name)
            {
                weapons[i].weapon.gameObject.SetActive(true);
                objectToReturn = tankController.TurretController.gameObject;
                tankController.TurretController = weapons[i].weapon;
            }
            else if (weapons[i].weapon.gameObject.activeSelf)
                weapons[i].weapon.gameObject.SetActive(false);
        }

        return objectToReturn;
    }

    public GameObject GetBaseID(GameObject gameObject)
    {
        GameObject objectToReturn = null;

        for (int i = 0; i < bases.Length; i++)
        {
            if (bases[i].tankbase.name == gameObject.name)
            {
                bases[i].tankbase.gameObject.SetActive(true);
                //objectToReturn = bases[i].tankbase.gameObject;
                //tankController.TurretController = weapons[i].weapon;
            }
            else if (bases[i].tankbase.gameObject.activeSelf)
            {
                objectToReturn = bases[i].tankbase.gameObject;
                bases[i].tankbase.gameObject.SetActive(false);
            }               
        }

        return objectToReturn;
    }

    public int GetPanelID(GameObject objectinChest)
    {
        int objectToReturn = 0;

        for (int i = 0; i < weapons.Length; i++)
        {
            if (weapons[i].weapon.name == objectinChest.name)
                objectToReturn = i;
        }

        return objectToReturn;
    }

    public int GetPanelIDBase(GameObject objectinChest)
    {
        int objectToReturn = 0;

        for (int i = 0; i < bases.Length; i++)
        {
            if (bases[i].tankbase.name == objectinChest.name)
            {
                objectToReturn = i;
            }
 
        }

        return objectToReturn;
    }

    public int GetActualTurret()
    {
        turretIndex = 0;
        for(int i = 0; i < weapons.Length; i++)
        {
            if (weapons[i].weapon.gameObject.activeSelf)
                turretIndex = i;
        }

        return turretIndex;
    }

    public int GetActualBase()
    {
        baseIndex = 0;
        for (int i = 0; i < bases.Length; i++)
        {
            if (bases[i].tankbase.gameObject.activeSelf)
                baseIndex = i;
        }

        return baseIndex;
    }
}

[System.Serializable]
public class WeaponAvailable
{
    public TurretController weapon;
    public GameObject panelToShow;
    public Button equipButton;
}

[System.Serializable]
public class BaseAvailable
{
    public GameObject tankbase;
    public GameObject panelToShow;
    public Button equipButton;
}
