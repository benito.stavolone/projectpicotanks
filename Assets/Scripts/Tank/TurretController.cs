﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TurretController : MonoBehaviour
{
    [Header("Shooting")]

    [SerializeField] private float damage;
    [SerializeField] private float msBetweenShots = 100;
    [SerializeField] private AudioClip shotAudio;

    public float NextShotTime
    {
        get { return _nextShotTime; }
    }

    public float MsBetweenShots
    {
        get { return msBetweenShots; }
    }

    private float _nextShotTime;

    [Header("Reload")]

    [SerializeField] private Slider reloadBar;
    [SerializeField] private float Speed=10;

    [Header("Projectile")]

    [SerializeField] private BulletController projectile;
    [SerializeField] private int projectilePerMag;
    [SerializeField] private GameObject[] barProjectile;
    [SerializeField] private Transform[] projectileSpawn;
    [SerializeField] private float bulletVelocity = 35;
    [SerializeField] private Transform turretHolder;

    private float consumeEachShot;
    private int remnantProjectile;

    [Header("Recoil")]

    [SerializeField] private float kickRecoil = 0.05f;
    [SerializeField] private float angleRecoil = 5;

    [Header("Collision Mask")]

    [SerializeField] private LayerMask collisionMask;

    private Vector3 _recoilSmoothDampVelocity;
    private float _recoilAngle;
    private float _recoilRotSmoothDampVelocity;

    private bool _onTriggerReleaseSinceLastShot;

    [Header("Laser")]

    [SerializeField] private Transform laserOrigin;
    [SerializeField] private float Lenght = 10;
    private LineRenderer lr;
    private Vector3 destroyPoint;
    private int offset;

    [Header("MuzzleFlash")]

    [SerializeField] private Light muzzleLight;

    private void Start()
    {
        remnantProjectile = projectilePerMag;
        consumeEachShot = (100 / projectilePerMag);
        lr = GetComponent<LineRenderer>();
    }

    private void LateUpdate()
    {
        transform.localPosition = Vector3.SmoothDamp(transform.localPosition, Vector3.zero, ref _recoilSmoothDampVelocity, 0.1f);
        _recoilAngle = Mathf.SmoothDamp(_recoilAngle, 0, ref _recoilRotSmoothDampVelocity, 0.1f);
        transform.localEulerAngles = transform.localEulerAngles + Vector3.back * -_recoilAngle;

        if (reloadBar.value < 1)
            Reload();
    }

    public void OnTriggerHold()
    {
        Laser();
        _onTriggerReleaseSinceLastShot = true;
    }
    public void OnTriggerRelease()
    {
        DisableLineRenderer();
        Shoot();
        _onTriggerReleaseSinceLastShot = false;
    }

    public void DisableLineRenderer()
    {
        lr.enabled = false;
    }

    private void Shoot()
    {
        if (Time.time > _nextShotTime)
        {
            if (remnantProjectile > 0)
            {

                if (this.gameObject.tag == "Player")
                {
                    barProjectile[remnantProjectile - 1].SetActive(false);
                    reloadBar.value -= (consumeEachShot / 100);
                    SoundManager.instance.SoundEffect(shotAudio);
                }
                else
                    SoundManager.instance.EnemySoundEffect(shotAudio);

                for (int i = 0; i < projectileSpawn.Length; i++)
                {

                    _nextShotTime = Time.time + msBetweenShots / 1000;
                    BulletController newBullet = ObjectPool.instance.SpawnFromPool<BulletController>(projectile.name, projectileSpawn[i].position, projectileSpawn[i].rotation);
                    SetNewBulletValues(newBullet);
                    ObjectPool.instance.SpawnFromPool<Component>("MuzzleFlash", projectileSpawn[i].position, Quaternion.Euler(0,-90,90));
                    muzzleLight.enabled = true;
                    Invoke("ResetLight", 0.1f);
                }

                remnantProjectile--;

                transform.localPosition -= Vector3.forward * kickRecoil;
                _recoilAngle += angleRecoil;
                _recoilAngle = Mathf.Clamp(_recoilAngle, 0, 0);
            }
        }


        
    }

    private void ResetLight()
    {
        muzzleLight.enabled = false;
    }

    public void AutoAim(Quaternion rotation)
    {
        LayerMask layerWall = LayerMask.GetMask("Wall");
        RaycastHit rh;

        Collider[] enemy = Physics.OverlapSphere(transform.position, Lenght/2, collisionMask);

        foreach (Collider hit in enemy)
        {
            if (Physics.Raycast(transform.position, hit.transform.position - transform.position, out rh, Mathf.Infinity))
            {
                if (rh.collider.gameObject.layer != Mathf.Log(layerWall, 2))
                {
                    if(rh.collider.gameObject.GetComponent<EnemyParentController>() != null)
                    {
                        turretHolder.LookAt(new Vector3(rh.transform.position.x, transform.position.y, rh.transform.position.z));
                        Shoot();
                    }
                }
            }
        }
    }

    private void Laser()
    {
        lr.enabled = true;
        lr.SetPosition(0, laserOrigin.position);
        destroyPoint = laserOrigin.position + transform.forward * Lenght;
        lr.SetPosition(1, destroyPoint);
    }

    private void SetNewBulletValues(BulletController newBullet)
    {
        newBullet.CollisionMask = collisionMask;
        newBullet.Speed = bulletVelocity;
        newBullet.Damage = damage;
        newBullet.DestroyPoint = destroyPoint;
    }

    private void Reload()
    {
        float reloadSpeed = 1 / Speed;
        int projectileCurrent;
        float percent = reloadBar.value;
        while (percent < 1)
        {
            percent += Time.deltaTime * reloadSpeed;
            projectileCurrent = (int)((percent * 100) / consumeEachShot);

            if (projectileCurrent > remnantProjectile)
            {
                remnantProjectile++;
                barProjectile[remnantProjectile - 1].SetActive(true);
                return;
            }

            reloadBar.value = percent;
            return;
        }
    }

}
