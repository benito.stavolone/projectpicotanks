﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputTankController : MonoBehaviour
{

    [Header("Input Properties")]

    [SerializeField] private Camera camera;

    private Vector3 mousePosition;
    public Vector3 MousePosition
    {
        get { return mousePosition; }
    }

    private Vector3 mouseNormal;
    public Vector3 MouseNormal
    {
        get { return mouseNormal; }
    }

    private float forwardInput;
    public float ForwardInput
    {
        get { return forwardInput; }
    }

    private float rotationInput;
    public float RotationInput
    {
        get { return rotationInput; }
    }

    void Update()
    {
        if (camera)
        {
            HandleInput();
        }
    }

    protected virtual void HandleInput()
    {
        Ray screenray = camera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if(Physics.Raycast(screenray, out hit))
        {
            mousePosition = hit.point;
            mouseNormal = hit.normal;
        }

        forwardInput = Input.GetAxis("Vertical");
        rotationInput = Input.GetAxis("Horizontal");
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(mousePosition,0.5f);
    }
}
