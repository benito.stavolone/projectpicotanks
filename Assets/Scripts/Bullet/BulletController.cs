﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{
    private float _lifetime = 2;
    private LayerMask _destructionMask;

    private float _damage;
    public float Damage
    {
        set { _damage = value; }
    }

    private LayerMask _collisionMask;
    public LayerMask CollisionMask
    {
        set { _collisionMask = value; }
    }

    private float _speed;
    public float Speed
    {
        set { _speed = value; }
    }

    private Vector3 _destroyPoint;
    public Vector3 DestroyPoint
    {
        set { _destroyPoint = value; }
    }

    void Start()
    {
        if (_collisionMask != 8)
        {
            Collider[] initialCollision = Physics.OverlapSphere(transform.position, 0.01f, _collisionMask);
            if (initialCollision.Length > 0)
            {
                OnHitObject(initialCollision[0], transform.position);
            }
        }
        _destructionMask = LayerMask.GetMask("Wall");
        Invoke("ResetBullet", _lifetime);
    }

    void Update()
    {
        float moveDistance = _speed * Time.deltaTime;
        CheckCollision(moveDistance);
        transform.Translate(Vector3.forward * moveDistance);
        CheckDestroyPoint();
    }
    private void OnHitObject(Collider collision, Vector3 hitPoint)
    {
        IDamageable damageableObject = collision.GetComponent<IDamageable>();
        if (damageableObject != null)
        {
            damageableObject.TakeHit(_damage, hitPoint, transform.forward);
        }
        ResetBullet();
    }
    private void CheckCollision(float moveDistance)
    {
        Ray ray = new Ray(transform.position, transform.forward);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, moveDistance, _collisionMask, QueryTriggerInteraction.Collide))
        {
            OnHitObject(hit.collider, hit.point);
        }
        else if (Physics.Raycast(ray, out hit, moveDistance, _destructionMask, QueryTriggerInteraction.Collide))
        {
            ResetBullet();
        }
    }

    private void CheckDestroyPoint()
    {
        
        if (Vector3.Distance(transform.position, _destroyPoint) < 1f)
        {
            ResetBullet();
        }
            
    }
    private void ResetBullet()
    {
        if(gameObject.activeSelf)
            ObjectPool.instance.SpawnFromPool<Component>("BulletExplosion", transform.position, Quaternion.identity);
        gameObject.SetActive(false);
    }
}
