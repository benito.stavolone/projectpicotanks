﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyNormalController : EnemyParentController
{
    private enum enemyStatus { Idle, Patrol, Attack }

    private enemyStatus currentStatus = enemyStatus.Idle;

    [Header("Shooter Settings")]

    [SerializeField] private TurretController equipedGun;
    [SerializeField] private Transform fireSpawn;

    [Header("Patrol")]

    [SerializeField] private Transform[] moveSpots;

    private int randomSpot;
    private float waitTime;
    private float startWaitTime = 2;

    private bool isHide = false;

    [Header("Distance Change Status")]

    [SerializeField] private float distanceFromPlayer = 40;
    [SerializeField] private float retreatDistance = 15;
    [SerializeField] private float stoppingDistance = 25;

    private bool isSpawned = false;
    private Transform randomMoveSpots;

    new void Start()
    {
        base.Start();

        randomSpot = Random.Range(0,moveSpots.Length);
        waitTime = startWaitTime;
    }
    void Update()
    {
            Attack();
            Patrol();
    }

    private void Patrol()
    {
        if (currentStatus == enemyStatus.Patrol)
        {
            if(moveSpots.Length > 0)
            {
                transform.position = Vector3.MoveTowards(transform.position, moveSpots[randomSpot].position, speed * Time.deltaTime);

                if (Vector3.Distance(transform.position, moveSpots[randomSpot].position) < 1f)
                {
                    if (waitTime <= 0)
                    {
                        waitTime = startWaitTime;
                        randomSpot = Random.Range(0, moveSpots.Length);
                    }
                    else
                        waitTime -= Time.deltaTime;
                }

                if (Vector3.Distance(transform.position, moveSpots[randomSpot].position) > 2)
                    transform.LookAt(moveSpots[randomSpot]);
            }               
        }


    }

    IEnumerator SearchPlayer()
    {
        float refreshRate = 5f;
        LayerMask layerWall = LayerMask.GetMask("Wall");

        while (currentStatus == enemyStatus.Patrol)
        {
            Collider[] colliders = Physics.OverlapSphere(transform.position, distanceFromPlayer);

            foreach (Collider collider in colliders)
            {
                if (collider.gameObject.layer == Mathf.Log(layer, 2))
                {
                    OnChangeGamestate(enemyStatus.Attack);
                }
            }

            yield return new WaitForSeconds(refreshRate);
        }

        yield return null;
    }

    private void Attack()
    {
        RaycastHit hit;

        if (_target != null)
        {
            if (currentStatus == enemyStatus.Attack)
            {
                if (!isSpawned)
                {
                    transform.LookAt(_target);

                    if (Physics.Raycast(transform.position, transform.forward, out hit, distanceFromPlayer))
                    {
                        if (hit.collider.gameObject.layer == Mathf.Log(layer.value, 2))
                            UpdateDistance();                      
                    }
                }
                else
                {
                    transform.LookAt(_target);
                    UpdateDistance();
                }

            }
        }
        else
            OnChangeGamestate(enemyStatus.Patrol);
        
    }

    private void UpdateDistance()
    {
        if (!isSpawned)
        {
            if (Vector3.Distance(transform.position, _target.position) > stoppingDistance)
            {
                transform.position = Vector3.MoveTowards(transform.position, _target.position, speed * Time.deltaTime);
            }
            else if (Vector3.Distance(transform.position, _target.position) < stoppingDistance &&
                Vector3.Distance(transform.position, _target.position) > retreatDistance)
            {
                transform.position = this.transform.position;
            }
            else if (Vector3.Distance(transform.position, _target.position) < retreatDistance)
            {
                transform.position = Vector3.MoveTowards(transform.position, _target.position, -speed * Time.deltaTime);
            }
        }
        else
        {
            if (Vector3.Distance(transform.position, randomMoveSpots.position) < 1f)
            {
                transform.LookAt(_target);
            }
            else
            {
                transform.LookAt(randomMoveSpots);
                transform.position = Vector3.MoveTowards(transform.position, randomMoveSpots.position, speed * Time.deltaTime);
            }

        }
            


        fireSpawn.LookAt(_target);
        equipedGun.OnTriggerRelease();
    }

    private void OnChangeGamestate(enemyStatus nextStatus)
    {
        currentStatus = nextStatus;

        switch (nextStatus)
        {
            case enemyStatus.Idle:
                break;
            case enemyStatus.Patrol:
                StartCoroutine(SearchPlayer());
                break;
            case enemyStatus.Attack:
                break;
        }
    }

    public override void TakeHit(float damage, Vector3 hitPoint, Vector3 hitDirection)
    {
        base.TakeHit(damage, hitPoint, hitDirection);

        OnChangeGamestate(enemyStatus.Attack);
    }

    private void OnBecameVisible()
    {
        if(!isSpawned)
            OnChangeGamestate(enemyStatus.Patrol);
    }

    private void OnBecameInvisible()
    {
        if(!isSpawned)
            OnChangeGamestate(enemyStatus.Idle);
    }

    protected override void OnMyDeath()
    {
        base.OnMyDeath();
    }

    public void SetDominioMoveSpots(Transform movespots)
    {
        randomMoveSpots = movespots;
    }

    public void IAMSpawned()
    {
        isSpawned = true;
        OnChangeGamestate(enemyStatus.Attack);

        distanceFromPlayer = 50;
    }




}
