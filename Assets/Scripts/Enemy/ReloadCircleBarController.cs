﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReloadCircleBarController : MonoBehaviour
{
    [SerializeField] private TurretController enemyReference;
    [SerializeField] private Image reloadImage;

    [SerializeField] private Color alertColor;
    [SerializeField] private Color normalColor;

    private float reloadTime;
    private float msBetweenShots;

    private void Start()
    {
        msBetweenShots = enemyReference.MsBetweenShots/1000;
    }

    private void Update()
    {
        reloadTime = enemyReference.NextShotTime;

        reloadImage.fillAmount = (((reloadTime - Time.time) / msBetweenShots) - ((reloadTime - Time.time) / msBetweenShots)*2)+1;
    }
}
