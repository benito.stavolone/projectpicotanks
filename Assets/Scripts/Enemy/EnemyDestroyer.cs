﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDestroyer : MonoBehaviour
{
    [SerializeField] GameObject father;

    private void OnDisable()
    {
        if(father!=null)
            Destroy(father.gameObject);
    }
}
