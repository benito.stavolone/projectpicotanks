﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyParentController : LivingEntity
{
    [Header("General Settings")]

    [SerializeField] protected float speed;
    [SerializeField] private float percentageToSpawnLife;
    [SerializeField] private AudioClip explosion;

    [Header("Player Reference")]

    [SerializeField] protected LayerMask layer;

    protected Transform _target;
    protected NavMeshAgent _pathfinder;
    protected LivingEntity _myLivingEntity;
    protected LivingEntity _targetEntity;

    protected ObjectPool _objectPool;

    protected bool hasTarget = false;

    protected new void Start()
    {
        base.Start();
        _objectPool = ObjectPool.instance;
        _target = GameObject.FindGameObjectWithTag("Player").transform;
        _myLivingEntity = GetComponent<LivingEntity>();
        _targetEntity = _target.GetComponent<LivingEntity>();
        _myLivingEntity.OnDeath += OnMyDeath;
        //_targetEntity.OnDeath += OnTargetDeath;
        hasTarget = true;
    }

    protected virtual void OnMyDeath()
    {
        ObjectPool.instance.SpawnFromPool<Component>("TankDestroy", transform.position + Vector3.up, Quaternion.Euler(-90,0,0));
        SoundManager.instance.ExplosionsoundEffect(explosion);
        Destroy(gameObject);
        SpawnHealth();
    }

    protected void OnTargetDeath()
    {
        //hasTarget = false;
    }

    public override void TakeHit(float damage, Vector3 hitPoint, Vector3 hitDirection)
    {
        base.TakeHit(damage, hitPoint, hitDirection);
        ObjectPool.instance.SpawnFromPool<Component>("TankDamage", hitPoint, Quaternion.LookRotation(hitDirection));
    }

    private void SpawnHealth()
    {
        int spawnHealthProbability = Random.Range(0, 101);
        if (spawnHealthProbability < percentageToSpawnLife)
            ObjectPool.instance.SpawnFromPool<Component>("Health", new Vector3(transform.position.x, 
                _target.position.y, transform.position.z), Quaternion.Euler(-90,0,0));       
    }
}
