﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossColliderTriggerController : MonoBehaviour
{
    public event EventHandler OnPlayerEnterTrigger;

    private void OnTriggerEnter(Collider other)
    {
        TankController player = other.GetComponent<TankController>();
        if (player != null)
        {
            OnPlayerEnterTrigger?.Invoke(this, EventArgs.Empty);
        }
    }
}
