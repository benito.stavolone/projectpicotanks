﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BossBattleController : MonoBehaviour
{
    public enum Stage
    {
        WaitingToStart,
        Stage1,
        Stage2,
        FinalStage
    }

    [SerializeField] BossColliderTriggerController colliderTrigger;
    [SerializeField] BossController enemyBoss;
    [SerializeField] LivingEntity bossEntity;
    [SerializeField] private GameObject entrance;
    [SerializeField] private GameObject spriteToShow;
    [SerializeField] private Slider bossSlider;

    private Stage stage;

    private void Awake()
    {
        stage = Stage.WaitingToStart;
    }

    private void Start()
    {
        colliderTrigger.OnPlayerEnterTrigger += ColliderTrigger_OnPlayerEnterTrigger;
        //gameManagerRef = GameManager.instance;
        enemyBoss.Damage += BossDamaged;
    }

    private void ColliderTrigger_OnPlayerEnterTrigger(object sender, System.EventArgs e)
    {
        StartBattle();
        //bossSlider.gameObject.SetActive(true);
        spriteToShow.SetActive(true);
        colliderTrigger.OnPlayerEnterTrigger -= ColliderTrigger_OnPlayerEnterTrigger;
        entrance.SetActive(true);
    }

    private void StartBattle()
    {
        StartNextStage();
    }

    private void BossDamaged()
    {
        switch (stage)
        {
            default:
            case Stage.Stage1:
                if (bossSlider.value < 0.7)
                {
                    bossSlider.value = 0.7f;
                    StartNextStage();
                }

                break;
            case Stage.Stage2:
                if (bossSlider.value < 0.35)
                {
                    bossSlider.value = 0.35f;
                    StartNextStage();
                }
                break;
            case Stage.FinalStage:
                break;
        }
    }

    private void StartNextStage()
    {
        switch (stage)
        {
            default:
            case Stage.WaitingToStart:
                stage = Stage.Stage1;
                break;
            case Stage.Stage1:
                stage = Stage.Stage2;
                enemyBoss.StaticPhase();
                break;
            case Stage.Stage2:
                stage = Stage.FinalStage;
                enemyBoss.StaticPhase();
                break;
            case Stage.FinalStage:
                break;
        }
    }




}
