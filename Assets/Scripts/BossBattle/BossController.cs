﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BossController : EnemyParentController
{
    [SerializeField] private BossColliderTriggerController colliderTrigger;
    [SerializeField] private BulletController projectile;
    [SerializeField] private LayerMask collisionMask;
    [SerializeField] private int damage;

    [SerializeField] private FadeImageController fadeController;

    [SerializeField] private GameObject particleSystemSmoke;

    [SerializeField] private AudioClip shoot;

    public PowerPerStage[] StagePower = new PowerPerStage[3];
    int powerStageIndex = 0;

    private LivingEntity targetEntity;
    private Transform target;

    private bool isStatic = false;

    private float timeBetweenShots;
    private float nextShotTime;
    private int indexStage = 0;
    private bool inShooting = false;
    private bool isVisible = false;
    private LivingEntity healthEntity;
    private int projectileRemaining;

    public System.Action Damage;

    protected void Start()
    {
        base.Start();
        target = GameObject.FindGameObjectWithTag("Player").transform;
        targetEntity = target.GetComponent<LivingEntity>();
        //targetEntity.OnDeath += OnTargetDeath;
        timeBetweenShots = StagePower[powerStageIndex].startTimeBtwShots;
        colliderTrigger.OnPlayerEnterTrigger += ColliderTrigger_OnPlayerEnterTrigger;
        healthEntity = transform.GetComponent<LivingEntity>();
    }

    private void Update()
    {
        if (isVisible)
        {
            if (!isStatic)
            {
                if (!inShooting)
                {
                    Shot();
                }
                else
                {
                    StartCoroutine(LookPlayer());
                }
                    
            }
            else
            {
                StagePower[indexStage].weaponToRotate.Active();
            }
        }
    }

    private void Shot()
    {
        for (int i = 0; i < StagePower[indexStage].projectileSpawn.Length; i++)
        {
            if (projectileRemaining == 0)
                break;

            projectileRemaining--;
            nextShotTime = Time.time + StagePower[indexStage].msBetweenShots / 1000;
            SoundManager.instance.EnemySoundEffect(shoot);
            BulletController newBullet = ObjectPool.instance.SpawnFromPool<BulletController>(projectile.name, StagePower[indexStage].projectileSpawn[i].position, StagePower[indexStage].projectileSpawn[i].rotation);
            SetNewBulletValues(newBullet);
        }

        if (projectileRemaining == 0)
        {
            inShooting = true;
            Invoke("Reload", StagePower[indexStage].reloadTime);
        }
    }

    private void Reload()
    {
        projectileRemaining = StagePower[indexStage].projectilePerMag;
        inShooting = false;
    }

    public override void TakeDamage(float damage)
    {
        base.TakeDamage(damage);
        Damage?.Invoke();
    }

    IEnumerator LookPlayer()
    {
        transform.LookAt(target);
        yield return new WaitForSeconds(4);
    }

    public void StaticPhase()
    {
        isStatic = true;
        particleSystemSmoke.SetActive(true);
        indexStage++;
        particleSystemSmoke.transform.localScale += Vector3.one;
        OnStageChanged();
    }

    private void OnStageChanged()
    {
        timeBetweenShots = StagePower[powerStageIndex].startTimeBtwShots;
        gameObject.layer = 10;
        Invoke("ResetStaticPhase", 5);
    }

    private void ResetStaticPhase()
    {
        isStatic = false;
        gameObject.layer = 9;
    }

    private void ColliderTrigger_OnPlayerEnterTrigger(object sender, System.EventArgs e)
    {
        projectileRemaining = StagePower[indexStage].projectilePerMag;
        isVisible = true;
        colliderTrigger.OnPlayerEnterTrigger -= ColliderTrigger_OnPlayerEnterTrigger;
    }

    private void SetNewBulletValues(BulletController newBullet)
    {
        newBullet.CollisionMask = collisionMask;
        newBullet.Speed = StagePower[indexStage].muzzleVelocity;
        newBullet.Damage = damage;
    }

    protected override void OnMyDeath()
    {
        base.OnMyDeath();
        fadeController.Startfade();
    }
}

[System.Serializable]
public class PowerPerStage
{
    public float muzzleVelocity;
    public float startTimeBtwShots;
    public float reloadTime;
    public int projectilePerMag;
    public float msBetweenShots;
    public Transform[] projectileSpawn;
    public WeaponRotation weaponToRotate;
}
