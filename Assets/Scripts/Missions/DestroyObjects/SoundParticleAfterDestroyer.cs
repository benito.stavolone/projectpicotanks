﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundParticleAfterDestroyer : MonoBehaviour
{
    [SerializeField] private AudioClip explosion;

    private bool isQuitting = false;

    private void OnDestroy()
    {
        if (!isQuitting)
        {
            SoundManager.instance.ExplosionsoundEffect(explosion);
            ObjectPool.instance.SpawnFromPool<Component>("ExplosionBuilding", transform.position + Vector3.up, Quaternion.Euler(-90,0,0));
        }

    }

    private void OnApplicationQuit()
    {
        isQuitting = true;
    }

}
