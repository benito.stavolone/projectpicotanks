﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DestroyObjectsController : MonoBehaviour
{
    UIManager UIReference;

    [SerializeField] GameObject lightComponenet;
    [SerializeField] GameObject missionBlocks;

    [SerializeField] GameObject spriteMissionBackground;

    private int count = 0;

    [SerializeField] TextMeshProUGUI textCurerntLightHouse;
    [SerializeField] TextMeshProUGUI totalCurrentLightHouse;

    private void Start()
    {
        UIReference = UIManager.instance;
        count = lightComponenet.transform.childCount;
        totalCurrentLightHouse.text = " / " + count.ToString();
        StartCoroutine(ChildCount());
    }

    IEnumerator ChildCount()
    {
        float refreshRate = 2;

        while (count > 0)
        {
            count = lightComponenet.transform.childCount;
            textCurerntLightHouse.text = count.ToString();

            yield return new WaitForSeconds(refreshRate);
        }

        MissionComplete();
        yield return null;
    }

    private void Update()
    {
        if (spriteMissionBackground.activeInHierarchy)
        {
            if (GameManager.instance.currentMissionState != GameManager.MissionState.DestroyObjects)
                MissionComplete();
        }
    }

    private void MissionComplete()
    {
        spriteMissionBackground.SetActive(false);
        StopAllCoroutines();
        gameObject.SetActive(false);
        Destroy(missionBlocks);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            spriteMissionBackground.SetActive(true);
            GameManager.instance.ChangeCurrentMission(GameManager.MissionState.DestroyObjects);
        }
    }
}
