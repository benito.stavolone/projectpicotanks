﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DraggableObject : MonoBehaviour
{
    [SerializeField] private Transform draggableParent;

    GameManager gameManagerRef;

    private void Start()
    {
        gameManagerRef = GameManager.instance;
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            gameManagerRef.ChangeCurrentMission(GameManager.MissionState.DragObject);
            transform.SetParent(draggableParent);
            gameObject.GetComponent<BoxCollider>().isTrigger = true;
            transform.localPosition = Vector3.zero;
        }
    }
}
