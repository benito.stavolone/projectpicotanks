﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class DragObjectController : MonoBehaviour
{
    UIManager UIReference;

    [SerializeField] private GameObject[] finishPoint;
    [SerializeField] private GameObject spriteMissionContainer;

    private void Start()
    {
        UIReference = UIManager.instance;

    }

    private void OnTriggerEnter(Collider other)
    {
        if (GameManager.instance.currentMissionState == GameManager.MissionState.Null)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                spriteMissionContainer.SetActive(true);
                UIReference.SetMissiontext("Drag Object");
                GameManager.instance.ChangeCurrentMission(GameManager.MissionState.SearchingDragObject);
            }
        }
    }
}
