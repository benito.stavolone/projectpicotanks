﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragObjectIsArrived : MonoBehaviour
{
    [SerializeField] private GameObject DragHolder;
    [SerializeField] private GameObject objectToDrag;
    [SerializeField] private GameObject starterMission;
    [SerializeField] private GameObject spriteMissionContainer;
    [SerializeField] private GameObject missionObstacle;

    private int count = 0;

    private void OnTriggerEnter(Collider other)
    {
        if(GameManager.instance.currentMissionState == GameManager.MissionState.DragObject)
        {
            count = DragHolder.transform.childCount;
            if (count > 0)
            {
                MissionComplete();
            }
                
        }
    }

    private void MissionComplete()
    {
        Destroy(objectToDrag);
        Destroy(starterMission);
        Destroy(missionObstacle);
        spriteMissionContainer.SetActive(false);
        GameManager.instance.currentMissionState = GameManager.MissionState.Null;
        
    }
}
