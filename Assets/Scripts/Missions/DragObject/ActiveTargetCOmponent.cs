﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveTargetCOmponent : MonoBehaviour
{
    [SerializeField] private Target myTargetComponent;

    [SerializeField] private GameManager.MissionState myMission;

    private void Update()
    {
        if (GameManager.instance.currentMissionState == myMission)
            myTargetComponent.enabled = true;
        else
            myTargetComponent.enabled = false;
    }

    private void OnDisable()
    {
        myTargetComponent.enabled = false;
    }
}
