﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DefeatEnemyController : MonoBehaviour
{
    UIManager UIReference;

    [SerializeField] GameObject enemyComponent;
    [SerializeField] GameObject missionBlocks;

    [SerializeField] GameObject spriteMissionBackground;

    [SerializeField] TextMeshProUGUI currentNumberofEnemy;
    [SerializeField] TextMeshProUGUI totalNumberofEnenmy;

    private int count = 0;

    private void Start()
    {
        UIReference = UIManager.instance;
        count = enemyComponent.transform.childCount;
        totalNumberofEnenmy.text = " / " + count.ToString();
        StartCoroutine(ChildCount());
    }

    IEnumerator ChildCount()
    {
        float refreshRate = 2;

        while(count > 0)
        {
            count = enemyComponent.transform.childCount;
            currentNumberofEnemy.text = count.ToString();
            yield return new WaitForSeconds(refreshRate);
        }

        MissionComplete();
        yield return null;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            spriteMissionBackground.SetActive(true);
            UIReference.SetMissiontext("Clear Area");
        }
    }

    private void MissionComplete()
    {
        spriteMissionBackground.SetActive(false);
        UIReference.HideTextMissionsDefeat();
        StopAllCoroutines();
        gameObject.SetActive(false);
        Destroy(missionBlocks);
    }
}
