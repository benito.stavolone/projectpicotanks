﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowSprite : MonoBehaviour
{
    [SerializeField] private string tagSprite;

    private bool isQuitting = false;

    private void OnDisable()
    {
        if(!isQuitting)
        {
            SpriteMovement sprite = ObjectPool.instance.SpawnFromPool<SpriteMovement>(tagSprite, new Vector3(transform.position.x, transform.position.y + 2, transform.position.z), Quaternion.identity);
            sprite.StartToMove();
        }
            
    }

    private void OnApplicationQuit()
    {
        isQuitting = true;
    }


}
