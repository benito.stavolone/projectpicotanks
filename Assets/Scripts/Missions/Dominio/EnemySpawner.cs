﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    private bool canSpawn = true;
    private bool canStart = false;
    private EnemyNormalController enemyReference = null;

    [SerializeField] private Transform myMoveSpots;

    private void Update()
    {
        if(canStart)
        {
            if (canSpawn)
            {
                enemyReference = ObjectPool.instance.SpawnFromPool<EnemyNormalController>("Enemy", transform.position, Quaternion.identity);
                enemyReference.SetDominioMoveSpots(myMoveSpots);
                enemyReference.IAMSpawned();
                canSpawn = false;
                //Invoke("ResetCanSpawn", 15);
            }

            if (enemyReference == null)
                canSpawn = true;
        }


    }

    public void Spawn(bool value)
    {
        canStart = value;
    }

    private void ResetCanSpawn()
    {
        canSpawn = true;
    }

}
