﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CheckOutofDominioZone : MonoBehaviour
{
    [SerializeField] private ActiveTargetCOmponent activeTargetComponenet;
    [SerializeField] private Slider dominioBar;
    [SerializeField] private float speed;

    [SerializeField] private GameObject dominiospriteContainer;
    [SerializeField] private PendulumAnimationController[] mybridges;

    [SerializeField] private EnemySpawner[] enemySpawnPoint;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            activeTargetComponenet.enabled = false;
            SpawnEnemy(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            activeTargetComponenet.enabled = true;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            LoadBar();
        }
    }

    private void LoadBar()
    {
        float reloadSpeed = 1 / speed;
        float percent = dominioBar.value;
        while (percent < 1)
        {
            percent += Time.deltaTime * reloadSpeed;
            dominioBar.value = percent;
            return;
        }

        MissionComplete();
    }

    private void SpawnEnemy(bool value)
    {
        for(int i = 0; i < enemySpawnPoint.Length;i++)
        {
            enemySpawnPoint[i].Spawn(value);
        }
    }

    private void MissionComplete()
    {
        SpawnEnemy(false);
        dominiospriteContainer.SetActive(false);
        for (int i = 0; i < mybridges.Length; i++)
            mybridges[i].Active();
        GameManager.instance.currentMissionState = GameManager.MissionState.Null;
        gameObject.SetActive(false);
    }
}
