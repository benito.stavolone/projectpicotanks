﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DominioController : MonoBehaviour
{
    [SerializeField] private GameObject dominiospriteContainer;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            dominiospriteContainer.SetActive(true);
            GameManager.instance.currentMissionState = GameManager.MissionState.Dominio;
        }
    }
}
