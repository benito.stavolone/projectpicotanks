﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveBridge : MonoBehaviour
{
    [SerializeField] private PendulumAnimationController[] myBridge; 

    private void OnDisable()
    {
        for(int i = 0; i < myBridge.Length; i++)
        {
            myBridge[i].Active();
        }
    }
}
